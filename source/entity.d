module entity;

import std.stdio,
	std.math;

import dsfml.graphics;

import physics;

///
class Entity : Drawable
{
public:
	///
	this(string name,
		Vector2f position,
		real mass,
		float radius,
		Vector2f initVel = Vector2f(0, 0),
		Color color = Color.White)
	in {
		assert(mass >= 0);
		assert(radius >= 0);
	}
	body {
		this.name = name;

		_shape = new CircleShape(radius, 30);
		_shape.origin = Vector2f(radius, radius);
		_shape.position = position;
		_shape.fillColor = color;

		this.mass = mass;

		velocity = initVel;
		acceleration = Vector2f(0, 0);
		_netForce = Vector2f(0, 0);

		debug writefln("INIT " ~ name ~ ": u = %s m/s, m = %s kg", velocity.magnitude.toMeters, mass);
	}

	///
	void update(float dt)
	{
		acceleration = _netForce / mass;
		velocity += acceleration * dt;
/*
		debug
		{
			string vdir = (velocity.y > 0 ? "D" : (velocity.y < 0 ? "U" : ""))
						~ (velocity.x > 0 ? "R" : (velocity.x < 0 ? "L" : ""));
			string adir = (acceleration.y > 0 ? "D" : (acceleration.y < 0 ? "U" : ""))
						~ (acceleration.x > 0 ? "R" : (acceleration.x < 0 ? "L" : ""));
			writefln(name ~ ": v = %s m/s " ~ vdir ~ ", a = %s m/s² " ~ adir,
				velocity.magnitude.toMeters, acceleration.magnitude.toMeters);
		}
*/
		_shape.move(velocity * dt);

		acceleration = Vector2f(0, 0);
		_netForce = Vector2f(0, 0);
	}

	///
	void interact(Entity entity)
	{
		Vector2f displacement = (entity._shape.position - this._shape.position).toMeters;
		Vector2f force = displacement; // giving the force's direction and orientation towards
										// the other entity and then setting its appropriate
		force.magnitude = gforce(this.mass, entity.mass, displacement.magnitude); // magnitude
		_netForce += force;
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		target.draw(_shape, states);
	}

	@property
	{
		///
		void position(Vector2f pos)
		{
			_shape.position = pos;
		}

		///
		Vector2f position() const
		{
			return _shape.position;
		}

		///
		void color(Color c)
		{
			_shape.fillColor = c;
		}

		///
		Color color() const
		{
			return _shape.fillColor;
		}
	}

	/// entity's name
	string name;

	// physical quantities
	/// mass of the entity
	real mass;
	/// velocity of motion
	Vector2f velocity;
	/// acceleration of motion
	Vector2f acceleration;

private:
	CircleShape _shape;
	Vector2f _netForce;
}
