module gui;

import dsfml.graphics;

import engine : unifont;

///
interface GuiComponent : Drawable
{
public:
	@property
	{
		///
		void position(Vector2f);
		///
		Vector2f position() const;
	}

	override void draw(RenderTarget, RenderStates);
}

///
class Tag : GuiComponent
{
public:
	///
	this()
	{
		_shape = new RectangleShape();
		_text = new Text("", unifont);

		_shape.outlineColor = Color.Black;
		_shape.outlineThickness(3);
		_shape.fillColor = Color(0xaa, 0xaa, 0xaa);

		_text.setColor(Color.White);
	}

	@property
	{
		override void position(Vector2f pos)
		{
			_shape.position = pos;
			_text.position = pos - Vector2f(2.5, 10);
		}

		override Vector2f position() const
		{
			return _shape.position;
		}

		///
		void text(string str)
		{
			_text.setString(str);
			auto bounds = _text.getGlobalBounds();
			_text.origin = Vector2f(bounds.width / 2, bounds.height / 2); 

			_shape.size = Vector2f(bounds.width + 10, bounds.height + 10);
			_shape.origin = _shape.size / 2;
		}

		///
		string text() const
		{
			return _text.getString();
		}

		///
		void color(Color c)
		{
			_shape.fillColor = Color(c.r, c.g, c.b, c.a / 2);
		}

		///
		Color color() const
		{
			return _shape.fillColor;
		}
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		target.draw(_shape);
		target.draw(_text);
	}

private:
	RectangleShape _shape;
	Text _text;
}
