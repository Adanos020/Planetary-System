module engine;

import std.stdio,
	std.string;

import dsfml.graphics;

import entity,
	gui,
	physics;

///
Font unifont;

static this()
{
	unifont = new Font();
	unifont.loadFromFile("unifont.ttf");
}

///
enum ActionState
{
	None,
	Panning,
	Selecting
}

///
struct Engine
{
public:
	///
	this(Vector2i wsize, RenderWindow window)
	{
		_wsize = wsize;

		_tag = new Tag();

		_worldView = new View(FloatRect(0, 0, wsize.x, wsize.y));
		_worldView.center = Vector2f(0, 0);

		_guiView = window.getDefaultView.dup;

		_zoomLevel = 1;
		_timeSpeedPower = 0;

		_simStatsText = new Text(format("time speed = 10^%sx\nzoom = %sx", _timeSpeedPower, _zoomLevel), unifont, 20);
		_simStatsText.setColor(Color.White);
		_simStatsText.position = Vector2f(0, 0);

		_window = window;

		_panningAnchor = Vector2i(-1, -1);

/*
		addEntity("Sun", Vector2f(0, 0),
			1.989e30, 696_300.kilometers, Vector2f(0, 0), Color(0xff, 0xaa, 0x00));
/*
		addEntity("Mercury", Vector2f(46_001_200.kilometers, 0),
			3.3011e23, 7.kilometers, Vector2f(0, 0), Color(0x00, 0xf, 0xff));
		addEntity("Venus", Vector2f(108_939_000.kilometers, 0),
			4.8675e24, 6051.8.kilometers, Vector2f(0, 0.0003502.kilometers), Color(0xaa, 0xff, 0xaa));

		addEntity("Earth", Vector2f(152_100_000.kilometers, 0),
			5.9722e24, 6371.kilometers, Vector2f(0, 0.0002978.kilometers), Color(0x88, 0x88, 0xff));

		addEntity("Mars", Vector2f(1.6660.astroUnits, 0),
			6.4171e23, 3389.5.kilometers, Vector2f(0, 0.00024077.kilometers), Color(0xff, 0x44, 0x00));

		addEntity("Jupiter", Vector2f(5.45492.astroUnits, 0),
			1.8986e27, 69_911.kilometers, Vector2f(0, 0.0001307.kilometers), Color(0x44, 0xdd, 0xff));

		addEntity("Saturn", Vector2f(10.086.astroUnits, 0),
			5.6836e26, 58_232.kilometers, Vector2f(0, 0.0000969.kilometers), Color(0xaa, 0xee, 0x22));

		addEntity("Uranus", Vector2f(20.11.astroUnits, 0),
			8.6810e25, 25_362.kilometers, Vector2f(0, 0.0000680.kilometers), Color(0xaa, 0xaa, 0xff));

		addEntity("Neptune", Vector2f(29.81.astroUnits, 0),
			1.0243e26, 24_622.kilometers, Vector2f(0, 0.0000543.kilometers), Color(0x33, 0x33, 0xff));*/

		addEntity(
			"Moon",
			Vector2f((4*3844 + 1_000).kilometers, 0),
			7.34767309e22,
			240.kilometers,
			Vector2f(0, 0.0000001022.kilometers),
			Color(0xdd, 0xdd, 0xdd)
		);

		addEntity(
			"Moon 2",
			Vector2f(1_000.kilometers, 3*3844.kilometers),
			7.34767309e22,
			240.kilometers,
			Vector2f(0.0000000511.kilometers, 0),
			Color(0xdd, 0xdd, 0xdd)
		);

		addEntity(
			/* name		 */ "Earth",
			/* position	 */ Vector2f(0, 0),
			/* mass (kg) */ 5.9722e24,
			/* radius	 */ 6371.kilometers,
			/* velocity	 */ Vector2f(0, 0),
			/* color	 */ Color(0x88, 0x88, 0xff)
		);
/+
		addEntity(
			/* name		 */ "Ċemr",
			/* position	 */ Vector2f(1.1 * 6371.kilometers, 0),
			/* mass (kg) */ 5.9722e24,
			/* radius	 */ 6371.kilometers,
			/* velocity	 */ Vector2f(0, -sqrt(G * 5.9722e24 / (2 * 1.1^^2 * 6_371_000)).meters),
			/* color	 */ Color(0x88, 0x88, 0xff)
		);+/
/*
		addEntity("Terra", Vector2f(-2 * 6371.kilometers, 6371.kilometers * sqrt(3.0)),
			5.9722e24, 6371.kilometers, Vector2f(0, 0), Color(0x88, 0x88, 0xff));*/
	}

	///
	void update(float dt)
	{
		dt *= 10^^_timeSpeedPower;

		foreach (i, entity1; _entities) foreach (j, entity2; _entities)
			if (i != j)
		{
			entity1.interact(entity2);
		}

		foreach (i, entity; _entities)
		{
			entity.update(dt);
		}
	}

	///
	void handleInput(ref Event event)
	{
		switch (event.type) with (Event.EventType)
		{
			case MouseButtonPressed:
			{
				if (event.mouseButton.button == Mouse.Button.Middle &&
					_astate != ActionState.Panning)
				{
					_astate = ActionState.Panning;
					_panningAnchor = Mouse.getPosition(_window);
				}
				break;
			}

			case MouseButtonReleased:
			{
				if (_astate == ActionState.Selecting || _astate == ActionState.Panning)
					_astate = ActionState.None;
				break;
			}

			case MouseWheelMoved:
			{
				if (event.mouseWheel.delta < 0)
				{
					_worldView.zoom(1.1);
					_zoomLevel *= 1.1;
					updateSimStats();
				}
				else
				{
					_worldView.zoom(1 / 1.1);
					_zoomLevel *= 1 / 1.1;
					updateSimStats();
				}
				break;
			}

			case MouseMoved:
			{
				if (_astate == ActionState.Panning)
				{
					Vector2f pos = Vector2f(Mouse.getPosition(_window) - _panningAnchor);
					_worldView.move(-pos * _zoomLevel);
					_panningAnchor = Mouse.getPosition(_window);
				}
				break;
			}

			case KeyPressed:
			{
				if (event.key.code == Keyboard.Key.Add)
				{
					++_timeSpeedPower;
					updateSimStats();
				}
				else if (event.key.code == Keyboard.Key.Subtract)
				{
					--_timeSpeedPower;
					updateSimStats();
				}
				break;
			}

			default: break;
		}
	}

	///
	void draw(RenderWindow window)
	{
		foreach (entity; _entities)
		{
			window.view = _worldView;
			window.draw(entity);

			window.view = _guiView;
			_tag.text = entity.name;
			_tag.position = Vector2f(0, -30) +
				(entity.position + _worldView.size / 2 - _worldView.center) / _zoomLevel;
			_tag.color = entity.color;
			window.draw(_tag);
		}
		window.draw(_simStatsText);
	}

private:
	void addEntity(string name,
				Vector2f position,
				real mass,
				float radius,
				Vector2f initVel = Vector2f(0, 0),
				Color color = Color.White)
	{
		_entities ~= new Entity(name, position, mass, radius, initVel, color);
	}

	void updateSimStats()
	{
		_simStatsText.setString(format("time speed = 10^%sx\nzoom = %sx", _timeSpeedPower, _zoomLevel));
	}

private:
	Entity[] _entities;

	Tag _tag;
	Text _simStatsText;

	RenderWindow _window;
	Vector2i _wsize;
	View _worldView;
	View _guiView;
	float _zoomLevel;
	float _timeSpeedPower;

	ActionState _astate = ActionState.None;
	Vector2i _panningAnchor;
}
