module app;

import std.stdio;

import dsfml.graphics;

import engine,
	physics;

///
struct App
{
public:
	///
	this(Vector2i wsize)
	{
		_window = new RenderWindow(VideoMode(wsize.x, wsize.y, 32),
			"Planetary System Sim", Window.Style.Close);
		_window.setVerticalSyncEnabled(true);
		_engine = Engine(wsize, _window);
		mainLoop();
	}

private:
	void mainLoop()
	{
		float tick = 1 / 60.0;
		float lag = 0.0;

		Clock clock = new Clock();
		while (_window.isOpen)
		{
			lag += clock.restart().sec;
			while (lag >= tick)
			{
				_engine.update(1.0e-6);
				lag -= 1.0e-6;
			}

			_window.clear();
			_engine.draw(_window);
			_window.display();

			handleInput();
		}
	}

	void handleInput()
	{
		Event event;
		while (_window.pollEvent(event))
		{
			if (event.type == Event.EventType.Closed)
				_window.close();
			_engine.handleInput(event);
		}
	}

private:
	RenderWindow _window;
	Engine _engine;
}

void main()
{
	try
	{
		App app = App(Vector2i(1280, 720));
	}
	catch (Exception ex)
	{
		stderr.writeln(ex.msg);
	}
}
