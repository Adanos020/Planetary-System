module physics;

import core.time;

import dsfml.graphics;

// constants

/// gravitational constant
enum G = 6.67e-11;


// time conversions

/// returns duration in seconds
float sec(Duration dt)
{
	return dt.total!"usecs" / 1_000_000.0;
}

/// returns duration in milliseconds
float msec(Duration dt)
{
	return dt.total!"usecs" / 1000.0;
}

/// returns duration in microseconds
float usec(Duration dt)
{
	return dt.total!"usecs";
}


// distance conversions

/// converts meters to pixels
float meters(float ds)
{
	return ds * 0.000001;
}

/// converts pixels to meters
float toMeters(float ds)
{
	return ds * 1_000_000;
}

/// converts meters to pixels
float kilometers(float ds)
{
	return ds * 0.001;
}

/// converts pixels to meters
float toKilometers(float ds)
{
	return ds * 1000;
}

/// converts astronomical units to pixels
float astroUnits(float ds)
{
	return meters(ds * 1.5e11);
}

/// converts pixels to astronomical units
float toAstroUnits(float ds)
{
	return toMeters(ds) / 1.5e11;
}

/// converts light years to pixels
float lightYears(float ds)
{
	return meters(ds * 9.46e15);
}

/// converts pixels to light years
float toLightYears(float ds)
{
	return toMeters(ds) / 9.46e15;
}

/// converts parsecs to pixels
float parsecs(float ds)
{
	return lightYears(ds * 3.26);
}

/// converts pixels to parsecs
float toParsecs(float ds)
{
	return toLightYears(ds) / 3.26;
}


// vector math

/// converts meters to pixels
Vector2f meters(Vector2f ds)
{
	return ds * 0.000001;
}

/// converts pixels to meters
Vector2f toMeters(Vector2f ds)
{
	return ds * 1_000_000;
}

/// converts meters to pixels
Vector2f kilometers(Vector2f ds)
{
	return ds * 0.001;
}

/// converts pixels to meters
Vector2f toKilometers(Vector2f ds)
{
	return ds * 1000;
}

/// converts astronomical units to pixels
Vector2f astroUnits(Vector2f ds)
{
	return meters(ds * 1.5e11);
}

/// converts pixels to astronomical units
Vector2f toAstroUnits(Vector2f ds)
{
	return toMeters(ds) / 1.5e11;
}

/// converts light years to pixels
Vector2f lightYears(Vector2f ds)
{
	return meters(ds * 9.46e15);
}

/// converts pixels to light years
Vector2f toLightYears(Vector2f ds)
{
	return toMeters(ds) / 9.46e15;
}

/// converts parsecs to pixels
Vector2f parsecs(Vector2f ds)
{
	return lightYears(ds * 3.26);
}

/// converts pixels to parsecs
Vector2f toParsecs(Vector2f ds)
{
	return toLightYears(ds) / 3.26;
}

/// calculates a vector's magnitude
float magnitude(Vector2f vec)
{
	return sqrt(vec.x^^2 + vec.y^^2);
}

/// sets a vector's magnitude keeping it's orientation
void magnitude(Vector2f vec, float newMagn)
{
	vec /= vec.magnitude;
	vec *= newMagn;
}


// gravitational field calculations

/// gravitational field potential
real potential(real M, real r)
{
	return -(G * M / r);
}

/// gravitational field strength
real strength(real dV, real dr)
{
	return -(dV / dr);
}

/// gravitational force
real gforce(real m1, real m2, real r)
{
	return (G * m1 * m2) / (r^^2);
}
